# -*- coding: utf-8 -*-
"""
    Модель для сохранения общих настроек событий сотрудников фирмы

    :copyright: (c) 2016 by Denis Amelin.
    :license: BSD, see LICENSE for more details.
"""

from web import db

from models.base_model import BaseModel


class CommonEvent(db.Model, BaseModel):

    __bind_key__ = 'term'
    __tablename__ = 'common_event'
    
    STATUS_NEW = 'new'
    STATUS_CEREATED = 'created'
    STATUS_REMOVING = 'removing'
    STATUS_REMOVED = 'removed'

    id = db.Column(db.Integer, primary_key=True)
    term_id = db.Column(db.Integer, db.ForeignKey('term.id'))
    term = db.relationship('Term')
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    event = db.relationship('Event')
    firm_id = db.Column(db.Integer, db.ForeignKey('firm.id'))
    firm = db.relationship('Event')
    timeout = db.Column(db.Integer, nullable=False)
    status = db.Column(db.String(32), nullable=False)
    

    def __init__(self):
        self.timeout = 5
        self.status = self.STATUS_NEW
        
    def to_json(self):
        items = dict(
            id=self.id,
            term_id=self.term_id,
            event_id=self.event_id,
            firm_id=self.firm_id,
            timeout=self.timeout,
            status=self.status,
        )
        return items