# -*- coding: utf-8 -*-
"""
    Модель комиссии за операции

    :copyright: (c) 2016 by Denis Amelin.
    :license: BSD, see LICENSE for more details.
"""
from web import db

from datetime import timedelta

from models.base_model import BaseModel
from models.firm import Firm
from models.term import Term
from models.firm_term import FirmTerm
from models.report import Report


class Commission(db.Model, BaseModel):

    __bind_key__ = 'term'
    __tablename__ = 'commission'
    
    TIME_DELTA = 3 #hours. It needs, because sqlalchemy doesn't perform
                   #to set timezone at db level 

    id = db.Column(db.Integer, primary_key=True)
    report_id = db.Column(db.Integer, db.ForeignKey('report.id'))
    report = db.relationship('Report')
    firm_id = db.Column(db.Integer, db.ForeignKey('firm.id'))
    firm = db.relationship('Firm')
    creation_date = db.Column(db.DateTime, nullable=False)
    amount = db.Column(db.Integer)

    def __init__(self, report):
        self.report_id = report.id
        self.firm_id = self.determine_firm(report)
        self.creation_date = report.creation_date + timedelta(hours=Commission.TIME_DELTA)
        self.amount = self.calculate(report, self.firm_id)

    @staticmethod
    def calculate(report, firm_id):
        amount = 0
        firm = Firm.query.get(firm_id)
        if not firm:
            return 0

        if not Commission.check_need(report):
            return 0

        if firm.transaction_percent > 0:
            amount = float(report.amount) * \
                (float(firm.transaction_percent) / 100 / 100)
            if firm.transaction_comission > 0 and amount < firm.transaction_comission:
                amount = firm.transaction_comission

        elif firm.transaction_comission > 0:
            amount = firm.transaction_comission

        return amount

    @staticmethod
    def determine_firm(report):
        firm_term = Firm.query.get(report.term_firm_id)
        if not firm_term:
            return False

        if firm_term.commission_type == Firm.COMMISSION_BY_TERM:
            return firm_term.id

        firm_person = Firm.query.get(report.person_firm_id)

        if not firm_person:
            return False

        if firm_person.commission_type == Firm.COMMISSION_BY_PERSON:
            return firm_person.id

        return False

    @staticmethod
    def check_need(report):
        if not (
                report.status == Report.STATUS_COMPLETE
                and report.type == Report.TYPE_WHITE
                and report.corp_type == Report.CORP_TYPE_ON):

            return False

        term = Term.query.get(report.term_id)

        if not term or not term.has_comission:
            return False

        return True

    @staticmethod
    def set(report):
        exists = False
        commission = Commission.query.filter(
            Commission.report_id == report.id).first()

        if not commission:
            commission = Commission(report)
        else:
            commission.firm_id = Commission.determine_firm(report)
            commission.amount = Commission.calculate(
                report, commission.firm_id)
            commission.creation_date = report.creation_date + timedelta(hours=Commission.TIME_DELTA)
            exists = True

        if commission.amount > 0 and commission.firm_id:
            commission.save()
            return True
        elif exists:
            commission.delete()

        return False

    @staticmethod
    def set_by_report_id(report_id):
        report = Report.query.get(report_id)
        if not report:
            return False

        return Commission.set(report)
