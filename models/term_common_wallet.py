# -*- coding: utf-8 -*-
"""
    Модель для сохранения общих настроек корпоративного кошелька сотрудников фирмы

    :copyright: (c) 2016 by Denis Amelin.
    :license: BSD, see LICENSE for more details.
"""
from web import db

from models.base_model import BaseModel
from models.term_corp_wallet import TermCorpWallet

from helpers import date_helper

class CommonWallet(db.Model, BaseModel):

    __bind_key__ = 'term'
    __tablename__ = 'common_wallet'
    
    STATUS_NEW = 'new'
    STATUS_CEREATED = 'created'
    STATUS_REMOVING = 'removing'
    STATUS_REMOVED = 'removed'

    id = db.Column(db.Integer, primary_key=True)
    creation_date = db.Column(db.DateTime, nullable=False)
    limit = db.Column(db.Integer, nullable=False)
    interval = db.Column(db.Integer, nullable=False, index=True)
    firm_id = db.Column(db.Integer, db.ForeignKey('firm.id'))
    status = db.Column(db.String(32), nullable=False)

    def __init__(self):
        self.limit = 0
        self.interval = TermCorpWallet.INTERVAL_MONTH
        self.creation_date = date_helper.get_current_date()
        self.status = self.STATUS_NEW

    def to_json(self):
        corp_wallet_interval = TermCorpWallet().get_interval_list()
        items = dict(
            id=self.id,
            limit=self.limit / 100,
            interval=self.interval,
            interval_name=corp_wallet_interval[self.interval]['name'],
            status=self.status,
        )
        return items