"""add_tz_column_in_firm

Revision ID: 2cdac854fed2
Revises: 162e23914322
Create Date: 2016-02-18 12:12:52.139483

"""

# revision identifiers, used by Alembic.
revision = '2cdac854fed2'
down_revision = '162e23914322'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()



def upgrade_term():
    op.add_column('firm', sa.Column(
        'tz', sa.String(150), nullable=False, server_default=u'Europe/Moscow'))


def downgrade_term():
    op.drop_column('firm', 'tz')


def upgrade_stack():
    pass


def downgrade_stack():
    pass


def upgrade_payment():
    pass


def downgrade_payment():
    pass


def upgrade_mobispot():
    pass


def downgrade_mobispot():
    pass

