"""add rent_gprs_type to firm

Revision ID: 425392c24d74
Revises: 14fc5c4d2a2d
Create Date: 2016-07-08 12:18:19.137599

"""

# revision identifiers, used by Alembic.
revision = '425392c24d74'
down_revision = '14fc5c4d2a2d'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()



def upgrade_term():
    op.add_column('firm', sa.Column(
        'rent_gprs_type', sa.Integer(), nullable=True))


def downgrade_term():
    op.drop_column('firm', 'rent_gprs_type')


def upgrade_stack():
    pass


def downgrade_stack():
    pass


def upgrade_payment():
    pass


def downgrade_payment():
    pass


def upgrade_mobispot():
    pass


def downgrade_mobispot():
    pass

