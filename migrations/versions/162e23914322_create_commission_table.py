"""create commission table

Revision ID: 162e23914322
Revises: 285541867a4b
Create Date: 2015-12-22 15:16:58.076418

"""

# revision identifiers, used by Alembic.
revision = '162e23914322'
down_revision = '285541867a4b'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()


def upgrade_term():
    op.create_table(
        'commission',
        sa.Column('id', sa.Integer()),
        sa.Column('report_id', sa.Integer(), nullable=False),
        sa.Column('firm_id', sa.Integer(), nullable=False, index=True),
        sa.Column('creation_date', sa.DateTime, nullable=False, index=True),
        sa.Column('amount', sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.add_column('firm', sa.Column(
        'commission_type', sa.Integer(), nullable=True, server_default='0'))


def downgrade_term():
    op.drop_table('commission')
    op.drop_column('firm', 'commission_type')


def upgrade_stack():
    pass


def downgrade_stack():
    pass


def upgrade_payment():
    pass


def downgrade_payment():
    pass


def upgrade_mobispot():
    pass


def downgrade_mobispot():
    pass
