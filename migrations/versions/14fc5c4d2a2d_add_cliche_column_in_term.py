"""add cliche column in term

Revision ID: 14fc5c4d2a2d
Revises: 598a1f9f949
Create Date: 2016-05-16 15:13:38.863730

"""

# revision identifiers, used by Alembic.
revision = '14fc5c4d2a2d'
down_revision = '598a1f9f949'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()



def upgrade_term():
    op.add_column('term', sa.Column(
        'cliche', sa.String(255), nullable=True))


def downgrade_term():
    op.drop_column('term', 'cliche')


def upgrade_stack():
    pass


def downgrade_stack():
    pass


def upgrade_payment():
    pass


def downgrade_payment():
    pass


def upgrade_mobispot():
    pass


def downgrade_mobispot():
    pass

