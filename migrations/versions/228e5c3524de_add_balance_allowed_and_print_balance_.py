"""add_balance_allowed_and_print_balance_columns_to_term_event

Revision ID: 228e5c3524de
Revises: 2cdac854fed2
Create Date: 2016-03-02 11:04:24.675362

"""

# revision identifiers, used by Alembic.
revision = '228e5c3524de'
down_revision = '2cdac854fed2'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()


def upgrade_term():
    op.add_column('term_event', sa.Column(
        'balance_allowed', sa.Integer(), nullable=False, server_default='-2147483648'))
    op.add_column('term_event', sa.Column(
        'print_balance', sa.Integer(), nullable=False, server_default='0'))


def downgrade_term():
    op.drop_column('term_event', 'balance_allowed')
    op.drop_column('term_event', 'print_balance')


def upgrade_stack():
    pass


def downgrade_stack():
    pass


def upgrade_payment():
    pass


def downgrade_payment():
    pass


def upgrade_mobispot():
    pass


def downgrade_mobispot():
    pass

