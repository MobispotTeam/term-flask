"""add status column in common events

Revision ID: 598a1f9f949
Revises: 34da7c501e46
Create Date: 2016-03-22 08:20:03.392727

"""

# revision identifiers, used by Alembic.
revision = '598a1f9f949'
down_revision = '34da7c501e46'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()


def upgrade_term():
    op.add_column('common_event', sa.Column(
        'status', sa.String(32), nullable=False, server_default='new'))
    op.add_column('common_wallet', sa.Column(
        'status', sa.String(32), nullable=False, server_default='new'))


def downgrade_term():
    op.drop_column('common_event', 'status')
    op.drop_column('common_wallet', 'status')


def upgrade_stack():
    pass


def downgrade_stack():
    pass


def upgrade_payment():
    pass


def downgrade_payment():
    pass


def upgrade_mobispot():
    pass


def downgrade_mobispot():
    pass

