"""create common_event table

Revision ID: 34da7c501e46
Revises: 228e5c3524de
Create Date: 2016-03-14 07:25:16.974573

"""

# revision identifiers, used by Alembic.
revision = '34da7c501e46'
down_revision = '228e5c3524de'

from alembic import op
import sqlalchemy as sa


def upgrade(engine_name):
    eval("upgrade_%s" % engine_name)()


def downgrade(engine_name):
    eval("downgrade_%s" % engine_name)()


def upgrade_term():
    op.create_table(
        'common_event',
        sa.Column('id', sa.Integer()),
        sa.Column('term_id', sa.Integer(), nullable=False, index=True),
        sa.Column('event_id', sa.Integer(), nullable=False),
        sa.Column('firm_id', sa.Integer(), nullable=False, index=True),
        sa.Column('timeout', sa.Integer(), server_default='5'),
        sa.PrimaryKeyConstraint('id')
    )
    
    op.create_table(
        'common_wallet',
        sa.Column('id', sa.Integer()),
        sa.Column('creation_date', sa.DateTime(), nullable=False),
        sa.Column('firm_id', sa.Integer(), nullable=False, index=True),
        sa.Column('limit', sa.Integer(), nullable=False, server_default='0'),
        sa.Column('interval', sa.Integer(), nullable=False, index=True, server_default='0'),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade_term():
    op.drop_table("common_event")
    op.drop_table("common_wallet")


def upgrade_stack():
    pass


def downgrade_stack():
    pass


def upgrade_payment():
    pass


def downgrade_payment():
    pass


def upgrade_mobispot():
    pass


def downgrade_mobispot():
    pass

