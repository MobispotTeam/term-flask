# -*- coding: utf-8 -*-

"""
    Обработка общих настрек кошелька и событий

    :copyright: (c) 2016 by Amelin Denis.
    :license: BSD, see LICENSE for more details.
"""

from web.celery import celery
from models.term_common_wallet import CommonWallet
from models.common_event import CommonEvent
from models.person import Person
from models.person_event import PersonEvent
from models.term_corp_wallet import TermCorpWallet


class CommonOptionsProcessor(object):

    @staticmethod
    @celery.task
    def common_event_manager():
        select_status = (
            CommonEvent.STATUS_NEW,
            CommonEvent.STATUS_REMOVING)

        element_keys = CommonEvent.query.filter(
            CommonEvent.status.in_(select_status)).all()
        if not element_keys:
            return False

        for key in element_keys:
            if key.status == CommonEvent.STATUS_NEW:
                CommonOptionsProcessor.add_events.delay(key.id)
            elif key.status == CommonEvent.STATUS_REMOVING:
                CommonOptionsProcessor.remove_events.delay(key.id)

        return True

    @staticmethod
    @celery.task
    def common_wallet_manager():
        select_status = (
            CommonWallet.STATUS_NEW,
            CommonWallet.STATUS_REMOVING)

        element_keys = CommonWallet.query.filter(
            CommonWallet.status.in_(select_status)).all()
        if not element_keys:
            return False

        for key in element_keys:
            if key.status == CommonWallet.STATUS_NEW:
                CommonOptionsProcessor.add_wallets.delay(key.id)
            elif key.status == CommonWallet.STATUS_REMOVING:
                CommonOptionsProcessor.remove_wallets.delay(key.id)

        return True

    @staticmethod
    @celery.task
    def add_events(common_event_id):
        common_event = CommonEvent.query.get(common_event_id)
        if not common_event or common_event.status != CommonEvent.STATUS_NEW:
            return False

        persons = Person.query.filter_by(firm_id=common_event.firm_id).all()
        for person in persons:
            if not person.payment_id:
                continue

            person_event = PersonEvent.query.filter_by(
                person_id=person.id,
                term_id=common_event.term_id,
                event_id=common_event.event_id).first()

            if not person_event:
                person_event = PersonEvent()
                person_event.person_id = person.id
                person_event.term_id = common_event.term_id
                person_event.event_id = common_event.event_id
                person_event.firm_id = common_event.firm_id

            person_event.timeout = common_event.timeout

            person_event.save()
            person.save()

        common_event.status = CommonEvent.STATUS_CEREATED
        common_event.save()

        return True

    @staticmethod
    @celery.task
    def remove_events(common_event_id):
        common_event = CommonEvent.query.get(common_event_id)
        if not common_event or common_event.status != CommonEvent.STATUS_REMOVING:
            return False

        persons = Person.query.filter_by(firm_id=common_event.firm_id).all()
        for person in persons:
            person_event = PersonEvent.query.filter_by(
                person_id=person.id,
                term_id=common_event.term_id,
                event_id=common_event.event_id,
                firm_id=common_event.firm_id).first()

            if not person_event:
                continue

            person_event.delete()
            person.save()

        common_event.delete()

        return True

    @staticmethod
    @celery.task
    def add_wallets(common_wallet_id):
        common_wallet = CommonWallet.query.get(common_wallet_id)
        if not common_wallet or common_wallet.status != CommonWallet.STATUS_NEW:
            return False

        persons = Person.query.filter_by(firm_id=common_wallet.firm_id).all()
        for person in persons:
            if not person.payment_id:
                continue

            corp_wallet = TermCorpWallet.query.filter_by(
                person_id=person.id).first()

            if not corp_wallet:
                corp_wallet = TermCorpWallet()
                corp_wallet.person_id = person.id

            corp_wallet.balance = common_wallet.limit
            corp_wallet.interval = common_wallet.interval
            corp_wallet.limit = common_wallet.limit
            corp_wallet.status = TermCorpWallet.STATUS_ACTIVE

            person.wallet_status = Person.STATUS_VALID
            person.type = Person.TYPE_WALLET

            person_events = PersonEvent.get_by_person_id(person.id)
            for person_event in person_events:
                person_event.timeout = 0
                person_event.save()

            corp_wallet.save()
            person.save()

        common_wallet.status = CommonWallet.STATUS_CEREATED
        common_wallet.save()

        return True

    @staticmethod
    @celery.task
    def remove_wallets(common_wallet_id):
        common_wallet = CommonWallet.query.get(common_wallet_id)
        if not common_wallet or common_wallet.status != CommonWallet.STATUS_REMOVING:
            return False

        persons = Person.query.filter_by(firm_id=common_wallet.firm_id).all()
        for person in persons:
            if person.type == Person.TYPE_WALLET:
                person.type = Person.TYPE_TIMEOUT
                if person.manually_blocked == Person.STATUS_VALID:
                    person.wallet_status = Person.STATUS_VALID

            corp_wallet = TermCorpWallet.query.filter_by(
                person_id=person.id).first()
            if corp_wallet:
                corp_wallet.delete()

            person.save()

        common_wallet.delete()

        return True
