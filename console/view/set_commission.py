# -*- coding: utf-8 -*-
"""
    Консольное приложение для начисления комиссии

    :copyright: (c) 2016 by Denis Amelin.
    :license: BSD, see LICENSE for more details.
"""

from flask.ext.script import Command, Option

from datetime import datetime, timedelta
from helpers import date_helper

from models.report import Report
from models.commission import Commission


class SetCommission(Command):

    "Set commission for existing reports"

    option_list = (
        Option('--date', dest='date', help='Format yyyy-mm-dd'),
        Option('--term_firm_id', dest='term_firm_id', default=False),
        Option('--person_firm_id', dest='person_firm_id', default=False),
        Option('--report_id', dest='report_id', default=False),
    )

    def run(self, date, term_firm_id, person_firm_id, report_id):
        date = datetime.strptime(date, '%Y-%m-%d')
        interval_date = date - timedelta(days=20)
        interval = date_helper.get_date_interval(interval_date, 'month')

        query = Report.query
        query = query.filter(
            Report.creation_date.between(interval[0], interval[1]))

        if term_firm_id:
            query = query.filter(Report.term_firm_id == int(term_firm_id))
        if person_firm_id:
            query = query.filter(Report.person_firm_id == int(person_firm_id))
        if report_id:
            query = query.filter(Report.id == int(report_id))

        reports = query.all()
        for report in reports:
            Commission.set(report)

        print "%s reports from interval %s - %s processed"\
            % (query.count(), interval[0], interval[1])
